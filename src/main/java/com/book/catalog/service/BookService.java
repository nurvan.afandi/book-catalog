package com.book.catalog.service;

import java.util.List;

import com.book.catalog.dto.BookCreateDTO;
import com.book.catalog.dto.BookDetailDTO;

public interface BookService {
	
	public BookDetailDTO findBookDetailById(Long bookId);
	
	public List<BookDetailDTO> findBookListDetail();
	
	public void createNewBook(BookCreateDTO dto);
	
}
