package com.book.catalog.repository;

import java.util.List;

import com.book.catalog.domain.Book;

public interface BookRepository {
	public Book findBookById(Long id);
	
	public List<Book> findAll();
	
	public void save(Book book);

}
