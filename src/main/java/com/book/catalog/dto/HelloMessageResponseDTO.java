package com.book.catalog.dto;

import lombok.Data;

@Data
public class HelloMessageResponseDTO {
	
	private String message;

}
